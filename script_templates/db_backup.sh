#!/bin/sh
 
backDatabase() {
    echo "backing up $3"
    mysqldump -h db_blog -u$1 -p$2 $3 > $4/$3-$5.sql
    gzip -f $4/$3-$5.sql
}
 
echo "start backup"
 
database=$DB_NAME
user=$MYSQL_USER
password=$MYSQL_PASSWORD
backupPath="/var/backup/db"
 
echo "deleting files older than 5 days"
find ${backupPath} -type f -mtime +5 -exec rm  {} \;
 
backDatabase ${user} ${password} ${database} ${backupPath} $(date +"%m-%d-%y")